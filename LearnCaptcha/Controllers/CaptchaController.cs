﻿using DNTCaptcha.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace LearnCaptcha.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class CaptchaController : ControllerBase
    {
        private readonly ILogger<CaptchaController> _logger;

        private readonly IDNTCaptchaValidatorService _validatorService;
        private readonly IDNTCaptchaApiProvider _captchaProvider;
        private readonly DNTCaptchaOptions _captchaOptions;

        public CaptchaController(
            ILogger<CaptchaController> logger,
            IDNTCaptchaValidatorService validatorService,
            IDNTCaptchaApiProvider captchaProvider,
            IOptions<DNTCaptchaOptions> options)
        {
            _logger = logger;
            _validatorService = validatorService;
            _captchaProvider = captchaProvider;
            _captchaOptions = options.Value;
        }

        [HttpGet]
        public IActionResult GetCaptcha()
        {
            /*
            var res = _captchaProvider.CreateDNTCaptcha(new DNTCaptchaTagHelperHtmlAttributes
            {
                ValidationErrorMessage = "Please enter the security code as a number.",
                Language = Language.English,
                FontName = "Arial",
                DisplayMode = DisplayMode.SumOfTwoNumbers,
                Min = 1,
                Max = 10,
                UseNoise = true,
                UseRelativeUrls = false,
                FontSize = 128,
            });
            */

            var res = _captchaProvider.CreateDNTCaptcha(new DNTCaptchaTagHelperHtmlAttributes
            {
                ValidationErrorMessage = "Please enter the security code as a number.",
                Language = Language.English,
                FontName = "Arial",
                DisplayMode = DisplayMode.ShowDigits,
                Min = 100000,
                Max = 999999,
                UseNoise = true,
                UseRelativeUrls = false,
                FontSize = 128,
            });

            return Ok(res);
        }

        [HttpPost]
        public IActionResult ValidateCaptcha([FromBody] RequestValidateCaptcha request)
        {
            var check = _validatorService.HasRequestValidCaptchaEntry(Language.English, DisplayMode.SumOfTwoNumbers);

            return Ok(check);
        }

        [HttpPost]
        public IActionResult ValidateCaptchaForm([FromForm] RequestValidateCaptcha request)
        {
            var check = _validatorService.HasRequestValidCaptchaEntry(Language.English, DisplayMode.ShowDigits);

            return Ok(check);
        }
    }

    public class RequestValidateCaptcha
    {
        public string DNTCaptchaText { get; set; }
        public string DNTCaptchaInputText { get; set; }
        public string DNTCaptchaToken { get; set; }
    }
}
